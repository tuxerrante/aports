# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjsembed
pkgver=5.102.0
pkgrel=0
pkgdesc="JavaScript bindings for QObject"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtsvg-dev kjs-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjsembed-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
b4434cd5c529d7c91e09f2da7fa8c6b80ca0676b9d1a670c54e61a12cf55460a467ada74231496c934cdb7671c8c658a29823c6be37b75d253fe94c3a0b5a6ed  kjsembed-5.102.0.tar.xz
"
