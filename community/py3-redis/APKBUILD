# Maintainer: Eivind Uggedal <eu@eju.no>
pkgname=py3-redis
_pkgname=redis
pkgver=4.4.2
pkgrel=0
pkgdesc="Python3 client for Redis key-value store"
url="https://github.com/andymccurdy/redis-py"
arch="noarch"
license="MIT"
depends="python3 py3-deprecated py3-hiredis"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
checkdepends="py3-pytest py3-mock redis"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # tests fail due to old version + disabled proprietary redis features

replaces="py-redis" # Backwards compatibility
provides="py-redis=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	redis-server --dir "$builddir" &
	trap "kill $!" EXIT
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
19a8ea299e363e94bfe87ada44f44d10a5d5eb283c3e5cea156b15f7edceb39a09687763301255bd9684dabf2257ad3f5cafecd2bbda64e6ac619d9852ca7db5  redis-4.4.2.tar.gz
"
