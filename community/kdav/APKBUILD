# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdav
pkgver=5.102.0
pkgrel=0
pkgdesc="A DAV protocol implementation with KJobs"
url="https://community.kde.org/Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
license="LGPL-2.0-or-later"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdav-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
03e600f7fcbd51e7f20bc49abd16eb6ef71901e54595ffb42fec69b54348a6b5b91db299249ee2eae2e12f1b54499a662dad45322e9f1450e29f6e33e3729822  kdav-5.102.0.tar.xz
"
