# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libxfce4ui
pkgver=4.18.1
pkgrel=0
pkgdesc="Widgets library for the Xfce desktop environment"
url="https://xfce.org/"
arch="all"
license="GPL-2.0-only"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
depends_dev="startup-notification-dev intltool"
makedepends="$depends_dev gtk+3.0-dev libxfce4util-dev xfconf-dev
	glade-dev gtk-doc gobject-introspection-dev libgtop-dev
	libepoxy-dev"
source="https://archive.xfce.org/src/xfce/libxfce4ui/${pkgver%.*}/libxfce4ui-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib \
		--localstatedir=/var \
		--with-vendor-info="${DISTRO_NAME:-Alpine Linux}" \
		--disable-static \
		--enable-introspection \
		--enable-gtk-doc \
		--enable-glibtop \
		--enable-epoxy
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
3bd8a52755c8542ff6952bb91bf57203605991963865f3d88896850ac5b98aec649c229d8743c1d472e6ac19ada853c428c1706aad0613a951aca1c001d9977d  libxfce4ui-4.18.1.tar.bz2
"
