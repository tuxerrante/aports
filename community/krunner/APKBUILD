# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krunner
pkgver=5.102.0
pkgrel=0
pkgdesc="Framework for providing different actions given a string query"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kservice-dev
	plasma-framework-dev
	plasma-framework-dev
	qt5-qtbase-dev
	threadweaver-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/krunner-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Requires running dbus instance

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d1697ce5e9cd02d5bf43c11f3de74b9603522524f8ae860c5d3430e340c491d915966517414f9e2860f58d8f5ef20218c9a9ebbe981f5504bc34fd4eca7c4bcd  krunner-5.102.0.tar.xz
"
