# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeclarative
pkgver=5.102.0
pkgrel=0
pkgdesc="Provides integration of QML and KDE Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libepoxy-dev
	qt5-qtdeclarative-dev
	samurai
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# quickviewsharedengine requires OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "quickviewsharedengine"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bff8f30725b0bdaa8dec8ec3745d9270c8564ebf6090f5c5e1aa842340f6549f8ceeebe1f08f71fbae1968a4d27c4cd6042af00ea46e0f1361e1ca110456c2e4  kdeclarative-5.102.0.tar.xz
"
