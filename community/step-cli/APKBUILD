# Contributor: Adrian L Lange <alpine@p3lim.net>
# Contributor: Charles Wimmer <charles@wimmer.net>
# Contributor: Dermot Bradley <dermot_bradley@yahoo.com>
# Maintainer: Dermot Bradley <dermot_bradley@yahoo.com>
pkgname=step-cli
pkgver=0.23.1
pkgrel=0
pkgdesc="Zero trust swiss army knife that integrates with step-ca for automated certificate management"
url="https://github.com/smallstep/cli"
arch="all !riscv64" # ftbfs
license="Apache-2.0"
makedepends="
	bash
	go
	go-bindata
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/smallstep/cli/archive/refs/tags/v$pkgver.tar.gz
	01-fix-tests.patch
	"
builddir="$srcdir/cli-$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make build
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 autocomplete/bash_autocomplete \
		"$pkgdir"/usr/share/bash-completion/completions/step

	install -Dm644 autocomplete/zsh_autocomplete \
		"$pkgdir"/usr/share/zsh/site-functions/_step
}

sha512sums="
0474478b1254bdad7d7fa4f3961c49980de953b6b6bdd8329187c6f4e9a11d5b1c6501a953a1f5e87c58cea3bacd01f137ba7dda25148ed90d73c67f64779687  step-cli-0.23.1.tar.gz
ebf6f7091e9a8f12a403f8fab808e0269497dbb8ff461b4207efafb071a695d3196ce52b22373afa157310a2f7959de647a558a2b9402729b3ee66e6062b8127  01-fix-tests.patch
"
