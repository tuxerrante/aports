# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=libimagequant
pkgver=4.0.5
pkgrel=0
pkgdesc="Small, portable C library for high-quality conversion of RGBA images to 8-bit indexed-color (palette) images."
url="https://pngquant.org/lib/"
arch="all !s390x !riscv64" # rust
license="GPL-3.0-or-later"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cargo cargo-c"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/ImageOptim/libimagequant/archive/$pkgver.tar.gz
	Cargo.lock
	"
builddir="$srcdir/$pkgname-$pkgver/imagequant-sys"
options="net"

case "$CARCH" in
x86)
	# tests fail
	options="$options !check"
	;;
*)
	;;
esac

prepare() {
	default_prepare

	ln -sfv "$srcdir"/Cargo.lock ../Cargo.lock

	cargo fetch --locked
}

build() {
	cargo cbuild --release \
		--frozen \
		--prefix /usr \
		--library-type cdylib
}

check() {
	cargo test --frozen
}

package() {
	cargo cinstall --release \
		--frozen \
		--prefix /usr \
		--destdir "$pkgdir" \
		--library-type cdylib
}

sha512sums="
2a1772b1a045364637d79caa0431b29bf97f98b8514b96194181a48a2a3d17727e2d343a62440dab9fd7a8ee05b00955d9bb25c2f795455ad2b433a75884c584  libimagequant-4.0.5.tar.gz
9f4c848b5b19b73718a918db1f5e1b6a9c546a89ad95295fb34f44bd8dde54fe8a5b3b43344475dd6a5a821da7d14c17dd5eaa2cece12a81e611be3b8615950e  Cargo.lock
"
