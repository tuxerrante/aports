# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pipewire
pkgver=0.3.64
pkgrel=2
pkgdesc="Multimedia processing graphs"
url="https://pipewire.org/"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	alsa-lib-dev
	avahi-dev
	bash
	bluez-dev
	dbus-dev
	doxygen
	eudev-dev
	fdk-aac-dev
	glib-dev
	graphviz
	gst-plugins-base-dev
	gstreamer-dev
	jack-dev
	libcamera-dev
	libfreeaptx-dev
	liblc3-dev
	libusb-dev
	libx11-dev
	meson
	ncurses-dev
	pulseaudio-dev
	py3-docutils
	readline-dev
	roc-toolkit-dev
	sbc-dev
	vulkan-loader-dev
	xmltoman
	"
subpackages="
	$pkgname-dbg
	$pkgname-dev
	$pkgname-doc
	$pkgname-alsa
	$pkgname-pulse
	$pkgname-jack
	gst-plugin-pipewire:gst_plugin
	$pkgname-zeroconf
	$pkgname-spa-bluez
	$pkgname-spa-vulkan
	$pkgname-tools
	$pkgname-spa-tools:spa_tools
	$pkgname-libs
	$pkgname-lang
	"
install="$pkgname.post-upgrade"
source="https://gitlab.freedesktop.org/PipeWire/pipewire/-/archive/$pkgver/pipewire-$pkgver.tar.gz
	$pkgname-fix-crackling-avx.patch::https://gitlab.freedesktop.org/pipewire/pipewire/-/commit/ccedf1722ae44c97ec0d058b349494c67a0b56dc.patch
	$pkgname-fix-compilation-depr.patch::https://gitlab.freedesktop.org/pipewire/pipewire/-/commit/a979c0f43078cbfefa2ba614ee078579042d2de2.patch
	$pkgname-fix-stuttering-custom.patch::https://gitlab.freedesktop.org/pipewire/pipewire/-/commit/1d9640af5a7906620f214aa0a39c63128c8506a6.patch
	pipewire.desktop
	pipewire-launcher.sh
	"

case "$CARCH" in
	s390x)
		# libldac not available for big endian
		;;
	*)
		makedepends="$makedepends libldac-dev"
		;;
esac

case "$CARCH" in
	ppc64le|s390x|riscv64)
		# no webrtc-audio-processing
		;;
	*)
		makedepends="$makedepends webrtc-audio-processing-dev"
		subpackages="$subpackages $pkgname-echo-cancel:echo_cancel"
		;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Dlibjack-path=/usr/lib \
		-Dlibv4l2-path=/usr/lib \
		-Ddocs=disabled \
		-Dman=enabled \
		-Dgstreamer=enabled \
		-Dexamples=enabled \
		-Dffmpeg=disabled \
		-Dsystemd=disabled \
		-Dvulkan=enabled \
		-Dsdl2=disabled \
		-Dlibcamera=enabled \
		-Droc=enabled \
		-Dbluez5-codec-lc3=enabled \
		-Dsession-managers=[] \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -Dm644 "$srcdir"/pipewire.desktop -t "$pkgdir"/etc/xdg/autostart/
	install -Dm755 "$srcdir"/pipewire-launcher.sh "$pkgdir"/usr/libexec/pipewire-launcher
}

dev() {
	default_dev

	mv "$subpkgdir"/usr/lib/libjack* "$pkgdir"/usr/lib/
}

alsa() {
	pkgdesc="ALSA support for pipewire"
	provides="pulseaudio-alsa=$pkgver-r$pkgrel"
	provider_priority=1
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/alsa-lib
	amove usr/share/alsa/alsa.conf.d

	mkdir -p "$subpkgdir"/etc/alsa/conf.d
	cp -r \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/*.conf \
		"$subpkgdir"/etc/alsa/conf.d/
}

pulse() {
	pkgdesc="Pulseaudio support for pipewire"
	depends="
		pipewire-session-manager
		pulseaudio-utils
		"
	provides="pulseaudio=$pkgver-r$pkgrel pulseaudio-bluez=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/bin/pipewire-pulse
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-protocol-pulse.so
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-pulse-tunnel.so
	amove usr/share/pipewire/pipewire-pulse.conf

}

jack() {
	pkgdesc="JACK support for pipewire"
	depends="pipewire-session-manager"
	provides="jack=$pkgver-r$pkgrel"
	replaces="jack"

	amove usr/lib/libjack*
	amove usr/bin/pw-jack
	amove usr/lib/spa-*/jack/libspa-jack.so
	amove usr/share/pipewire/jack.conf
}

gst_plugin() {
	pkgdesc="Multimedia graph framework - PipeWire plugin"
	depends="pipewire-session-manager gst-plugins-base"

	amove usr/lib/gstreamer-1.0
}

echo_cancel() {
	pkgdesc="WebRTC-based echo canceller module for PipeWire"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-echo-cancel.so
}

zeroconf() {
	pkgdesc="$pkgdesc - Zeroconf support"
	depends=""
	provides="pulseaudio-zeroconf=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-zeroconf-discover.so
}

bluez() {
	pkgdesc="PipeWire BlueZ5 SPA plugin (Bluetooth)"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/spa-*/bluez5
}

vulkan() {
	pkgdesc="PipeWire Vulkan SPA plugin"
	depends=""

	amove usr/lib/spa-*/vulkan
}

tools() {
	pkgdesc="PipeWire tools"
	depends="$pkgname=$pkgver-r$pkgrel"
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/pw-*
}

spa_tools() {
	pkgdesc="PipeWire SPA tools"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/spa-*
}

sha512sums="
ce4bbd7ac5a95318154cd5716ef684d271b43bfd3e10677c300e2d74f1652a64cac63779bbba8534f9f9a7139cbbca4006d805d80dbdf5cdc189ab7d6cb19518  pipewire-0.3.64.tar.gz
652d6d89247136555ccbc78110702d9a326462bb230839597f0e076281ae0d52aaca98aa30a7e79ce2f9bdfc4e497b9f6bd6981755efce81823be7ff4e0784da  pipewire-fix-crackling-avx.patch
85f361b1f7a02bf694e6af6c0d9ca894b42edd1718b99ec36dbcd733e450c56e3ee5318f1b5c0425682b68f4537fa936458cb0703162abe52924ccd0c683e5e8  pipewire-fix-compilation-depr.patch
3be04c5672c4f0600f27211ae5e69fce755df8eaafca57cd33a4825fb9a91b19831eb699f9ab90021cf2178068fab6f49c40d76c3051f9132403e4fd595ade2c  pipewire-fix-stuttering-custom.patch
d5d8bc64e42715aa94296e3e26e740142bff7f638c7eb4fecc0301e46d55636d889bdc0c0399c1eb523271b20f7c48cc03f6ce3c072e0e8576c821ed1ea0e3dd  pipewire.desktop
e46939b8f903fe6b7421cd42d0746e669402d76afe3326401c186fefeb725e3c126a00ba9f315067d2535991134a24afd855752d757e9e52c20191b5d388f99b  pipewire-launcher.sh
"
