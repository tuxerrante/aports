# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=gallery-dl
pkgver=1.24.4
pkgrel=0
pkgdesc="CLI tool to download image galleries"
url="https://github.com/mikf/gallery-dl"
arch="noarch"
license="GPL-2.0-or-later"
depends="py3-requests python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest yt-dlp"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikf/gallery-dl/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build

	make man completion
}

check() {
	make test
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Install fish completion to the correct directory
	mv "$pkgdir"/usr/share/fish/vendor_completions.d "$pkgdir"/usr/share/fish/completions
}

sha512sums="
19285f9db2eba95252f13af176c314a318fd71503913ce6b54e4b31d0c41b1ca79498289531ca0b9d67630395c8212ba497c5c2a838f554c24d1672c5c16caf3  gallery-dl-1.24.4.tar.gz
"
