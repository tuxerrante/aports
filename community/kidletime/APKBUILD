# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kidletime
pkgver=5.102.0
pkgrel=0
pkgdesc="Monitoring user activity"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kidletime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# solidmttest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5aeddaf18d89c1ae5f9a23950a319d7d3659f713258e6297f1cb9a76c39514d434be80ec87e76c5fa892c445c69263742a13614ffcc5ae8a9c0cd47c57c43f08  kidletime-5.102.0.tar.xz
"
