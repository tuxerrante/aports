# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcoreaddons
pkgver=5.102.0
pkgrel=0
pkgdesc="Addons to QtCore"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	shared-mime-info
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcoreaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# klistopenfilesjobtest_unix requires >8000 open files
	local disabled_tests="klistopenfilesjobtest_unix|kdirwatch_qfswatch_unittest|kdirwatch_stat_unittest"
	case "$CARCH" in
		s390x) disabled_tests="$disabled_tests|kdirwatch_inotify_unittest" ;;
		x86_64) disabled_tests="$disabled_tests|kfileutilstest" ;; # Only fails on builders
	esac

	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "($disabled_tests)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e3c82fc770df184016b9c9eea8bebf5343ca3e5d004fe1b1f7be198e2dcb85dcb154ede23dfdf42b9ecd073abc87dc0a31ce589e0524226a0dad0e74671cdbb6  kcoreaddons-5.102.0.tar.xz
"
