# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=py3-tox
_pkgname=${pkgname#py3-*}
pkgver=4.3.5
pkgrel=0
pkgdesc="virtualenv management and test command line tool"
options="!check" #  Requires community/py3-pathlib2, and unpackaged flaky
url="https://tox.readthedocs.org/"
arch="noarch"
license="MIT"
depends="
	py3-cachetools
	py3-chardet
	py3-colorama
	py3-filelock
	py3-packaging
	py3-platformdirs
	py3-pluggy
	py3-pyproject-api
	py3-virtualenv
	python3
	"
makedepends="
	py3-gpep517
	py3-hatch-vcs
	py3-hatchling
	"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tox" # Backwards compatibility
provides="py-tox=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
142c1484598e61e7e3606d8f374b0847f1b7eb432506cf952e33dd7cce09e1471ff8a1baa394fedc3e2db6af020e8cab13c4cb84c5dfd3a671c6dbb270926b45  tox-4.3.5.tar.gz
"
