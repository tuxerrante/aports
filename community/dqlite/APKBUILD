# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=dqlite
pkgver=1.13.0
pkgrel=0
pkgdesc="Embeddable, replicated and fault tolerant SQL engine."
url="https://dqlite.io/"
arch="all"
license="LGPL-3.0"
depends="raft"
makedepends="libuv-dev sqlite-dev raft-dev autoconf automake libtool"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/canonical/dqlite/archive/v$pkgver.tar.gz
	fix-fcntl-header-path.patch
	disable-failing-test-due-to-name-collision.patch
	no-werror.patch
	"

prepare() {
	default_prepare
	autoreconf -i
}

build() {
	./configure  \
		--prefix=/usr
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/dqlite/LICENSE
}

sha512sums="
da56c622c75dcdc1df814d2b7acfeb08cb046fa7b74691d63575ddb3d30f91b441b8f58cc72466872b4c5e0028706b6505846d8f3d06583be844baefdd85f8a9  dqlite-1.13.0.tar.gz
e839579dda8ccdcefba248070938bf1ad49d31c0c073531aa754ab54c4fdf86a3f15e13ffb892fc89fb587a38dd3e3ee023eb9e3dfb87f37edac5bac861de0f0  fix-fcntl-header-path.patch
b27424770bba2e33746a205e21785e439a5c0fa3ddd0f4bc2d910a86f293db58b1be824040225c63ee0cf7af92a38fd80d89d78cec54acf70bfbb780cdd0d01f  disable-failing-test-due-to-name-collision.patch
3979f6476d819d67007381d1b3c29c1444973f9dc17e2d637eeb455404c1a1039f4191cf8a844639fb61aee2e3d0661b9c8b70383790920d79c14cf3482eec67  no-werror.patch
"
