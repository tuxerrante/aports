# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=atomicparsley
pkgver=20221229
_pkgtag=172126.d813aa6
pkgrel=0
pkgdesc="A command line program for reading, parsing and setting iTunes-style metadata in MPEG4 files"
url="https://atomicparsley.sourceforge.net/"
arch="all"
options="!check" # no test/check available
license="GPL-2.0-or-later"
makedepends="cmake automake autoconf libtool zlib-dev linux-headers"
source="$pkgname-$pkgver.tar.gz::https://github.com/wez/atomicparsley/archive/${pkgver}.${_pkgtag}.tar.gz"
builddir="$srcdir/$pkgname-${pkgver}.${_pkgtag}"

build() {
	cmake .
	cmake --build . --config Release
}

package() {
	install -Dm755 AtomicParsley "$pkgdir"/usr/bin/AtomicParsley
	ln -s AtomicParsley "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
eb6de26f71a977cf4512f0d4249c3e0ae9225b5aef42a4bbdaaa54b022985f277095b7019c741a26078d26fba07e8dea7bf117017d0ef43d630751873e6b4899  atomicparsley-20221229.tar.gz
"
