# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=sofia-sip
pkgver=1.13.11
pkgrel=0
pkgdesc="RFC3261 compliant SIP User-Agent library"
url="https://github.com/freeswitch/sofia-sip"
arch="all"
license="LGPL-2.1-only"
options="!check" #tests are broken
makedepends="automake autoconf libtool m4
glib-dev openssl-dev>3 lksctp-tools-dev"
checkdepends="check-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/freeswitch/sofia-sip/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare
	sh autogen.sh
}

build() {
	./configure \
		--prefix=/usr \
		--with-openssl \
		--enable-sctp \
		--enable-static=no \
		--disable-rpath
	make
}

check() {
	TPORT_DEBUG=9 TPORT_TEST_HOST=0.0.0.0 make check
}

package() {
	make DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	make doxygen
}
sha512sums="
349e11dde895a303c65b331cf18f84604201e9662d91d09821878ba28650ca32d9437a88b8cdb814833207d03f68689999b9618fb88b686a30b5e8da769bfbda  sofia-sip-1.13.11.tar.gz
"
