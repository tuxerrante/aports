# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=raspberrypi-bootloader
# To match Alpine kernel schedule, use master branch commit id rather than older stable tagged releases
# Keep by-the-date release numbering for consistency
_commit=76e9fa24526eb79428e236e165ea7a6d36ed0d2f
pkgver=1.20230117
pkgrel=0
pkgdesc="Bootloader files for the Raspberry Pi"
url="https://github.com/raspberrypi/firmware"
arch="armhf armv7 aarch64"
license="custom"
options="!check !strip !tracedeps !spdx"
source="$pkgname-$pkgver.tar.gz::https://github.com/raspberrypi/firmware/archive/$_commit.tar.gz"
subpackages="$pkgname-common $pkgname-experimental $pkgname-debug $pkgname-cutdown $pkgname-doc"
depends="$pkgname-common=$pkgver-r$pkgrel"

builddir="$srcdir/firmware-$_commit"

package() {
	local fw; for fw in bootcode.bin fixup.dat fixup4.dat start.elf start4.elf; do
		install -D "$builddir"/boot/$fw \
			"$pkgdir"/boot/$fw
	done
	install -Dm 644 "$builddir"/boot/LICENCE.broadcom \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

common() {
	pkgdesc="Common files used by Raspberry Pi bootloaders"
	depends=
	amove boot/bootcode.bin
}

experimental() {
	pkgdesc="Experimental firmware with additional codecs"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_x.elf start4x.elf fixup_x.dat fixup4x.dat; do
		install -D "$builddir"/boot/$fw \
				"$subpkgdir"/boot/$fw
	done
}

debug() {
	pkgdesc="Debug firmware"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_db.elf start4db.elf fixup_db.dat fixup4db.dat; do
		install -D "$builddir"/boot/$fw \
			"$subpkgdir"/boot/$fw
	done
}

cutdown() {
	pkgdesc="Cut-down firmware for lower memory settings"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_cd.elf start4cd.elf fixup_cd.dat fixup4cd.dat; do
		install -D "$builddir"/boot/$fw \
			"$subpkgdir"/boot/$fw
	done
}

sha512sums="
a0098bd043bac4638edcd9cd45398797ff7187e55f27dac3ce976917920b4a5fc47a12485a6b9da96bae3064514bf12cf1de27d44dafd84401998e518b8907f8  raspberrypi-bootloader-1.20230117.tar.gz
"
